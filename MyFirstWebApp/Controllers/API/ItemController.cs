﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using MyFirstWebApp.Models.DB;
using MyFirstWebApp.Models;
using DAL;
using MyFirstWebApp.Models.DTO;
using DAL.Models;
using System.Web.Http.Cors;

namespace MyFirstWebApp.Controllers.API
{
    public class ItemController : ApiController
    {

        private EntityRepository<Item> item_db;
        private EntityRepository<Location> location_db;

        //Constructor
        public ItemController()
        {
            ApplicationDBContext ctx = new ApplicationDBContext();
            item_db = new EntityRepository<Item>(ctx);
            location_db = new EntityRepository<Location>(ctx);
        }

        /// <summary>
        /// Gets item from DB based on Uri input
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(Item))]
        public async Task<IHttpActionResult> GetItem([FromUri]int ID)
        {
            Item result = await item_db.GetSingleOrDefaultWhere(o => o.ID == ID);

            if (result == null)
            {
                return NotFound();
            }
            else
            {
                ItemResponse resp = new ItemResponse();
                resp.ParseExisting(result);
                return Ok(resp);
            }
        }

        /// <summary>
        /// Get a paginated list of items
        /// </summary>
        /// <param name="page"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Item>))]
        public async Task<IHttpActionResult> GetItemList(int page = 1, int take = 10)
        {

            PaginatedList<Item> existing = await item_db.Paginate(
               page,   //offset by take*(page - 1)
               take,   //number of results
               o => o.ID);//order by ID

            return Ok(existing.ToPaginatedDto<ItemResponse, Item>());
        }
        /// <summary>
        /// Get a paginated list of items by location
        /// </summary>
        /// <param name="locationID"></param>
        /// <param name="page"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Item>))]
        [Route("api/location/{locationID}/item")]
        public async Task<IHttpActionResult> GetItemsByLocation( int locationID, int page = 1, int take = 3)
        {
            Location loc = await location_db.GetSingleOrDefaultWhere(o => o.ID == locationID);
            if (loc == null)
            {
                return NotFound();
            }
            else
            {
                PaginatedList<Item> pagedItems = await item_db.Paginate(
                page,                                   //offset by take*(page - 1) 
                take,                                   //number of results
                o => o.ID,                              //Order by ID
                o => o.LocationID == locationID         //Where clause
                );
                return Ok(pagedItems.ToPaginatedDto<ItemResponse, Item>());
            }
        }
        /// <summary>
        /// Create an item via Post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(Item))]
        public async Task<IHttpActionResult> createItem(ItemRequest request)
        {
          
            Item dbObj = request.ToDB();
            item_db.Add(dbObj);
            await item_db.Save();
            return CreatedAtRoute("DefaultAPI",
                new
                {
                    ID = dbObj.ID
                },
                dbObj);
        }
        /// <summary>
        /// Update an item in the item DB
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPatch]
        public async Task<IHttpActionResult> updateItem(int ID, ItemPatchRequest request)
        {
            Item selection = await item_db.GetSingleOrDefaultWhere(o => o.ID == ID);
            if (selection == null)
            {
                return NotFound();
            }
        
            selection = request.ToDB(selection);
            item_db.Edit(selection);
            await item_db.Save();
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }
        /// <summary>
        /// Remove an item from the item DB
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IHttpActionResult> deleteItem([FromUri] int ID)
        {
            Item selection = await item_db.GetSingleOrDefaultWhere(o => o.ID == ID);
            if (selection == null)
                return NotFound();
            item_db.Delete(selection);
            await item_db.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}