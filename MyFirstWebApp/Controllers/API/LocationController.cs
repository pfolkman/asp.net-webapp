﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.Entity;
using MyFirstWebApp.Models.DB;
using DAL;
using MyFirstWebApp.Models;
using System.Runtime.Serialization;
using MyFirstWebApp.Models.DTO;
using DAL.Models;
using System.Web.Http.Cors;

namespace MyFirstWebApp.Controllers.API
{
    public class LocationController : ApiController
    {
        private EntityRepository<Location> location_db;
        private EntityRepository<Item> item_db;

        public LocationController()
        {
            ApplicationDBContext ctx = new ApplicationDBContext();
            location_db = new EntityRepository<Location>(ctx);
            item_db = new EntityRepository<Item>(ctx);
        }
        /// <summary>
        /// Get a location by Uri ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(Location))]
        //Get /api/Item/id
        public async Task<IHttpActionResult> GetLocation([FromUri]int ID)
        {
            Location result = await location_db.GetSingleOrDefaultWhere(o => o.ID == ID);
            if (result == null)
            {
                return NotFound();
            }
            else
            {
                LocationResponse resp = new LocationResponse();
                resp.ParseExisting(result);
                return Ok(resp);
            }
        }
        /// <summary>
        /// Get a paginated list of locations
        /// </summary>
        /// <param name="page"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<Location>))]
        //Get /api/Item
        public async Task<IHttpActionResult> GetLocationList(int page =1, int take = 5)
        {
            PaginatedList<Location> existing = await location_db.Paginate(
                page,   //offset by take*(page -1 )
                take,   //number of results
                o => o.ID);//order by ID
            
            return Ok(existing.ToPaginatedDto<LocationResponse, Location>());
        }
        /// <summary>
        /// Create a location via Post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(Location))]
        public async Task<IHttpActionResult> createLocation(LocationRequest request)
        {
            Location dbObj = request.ToDB();
            location_db.Add(dbObj);
            await location_db.Save();
            return CreatedAtRoute("DefaultAPI",
                new
                {
                    ID = dbObj.ID
                },
                dbObj);
        }
        /// <summary>
        /// Update a location in the DB
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPatch]
        public async Task<IHttpActionResult> updateLocation(int ID, LocationPatchRequest request)
        {
            Location selection = await location_db.GetSingleOrDefaultWhere(o => o.ID == ID);
            if (selection == null)
            {
                return NotFound();
            }
            selection = request.ToDB(selection);
            location_db.Edit(selection);
            await location_db.Save();
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Remove a location from the DB. Items at that location have locationID nulled out
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IHttpActionResult> deleteLocation([FromUri] int ID)
        {
            Location location = await location_db.GetSingleOrDefaultWhere(o => o.ID == ID);
            if (location == null)
                return NotFound();

            //Null out locations of items at that location
            List<Item> items = await item_db.GetAllWhere(o => o.LocationID == ID);
            if ( items.Count > 0)
            {
                foreach (var i in items)
                {
                    i.LocationID = null;
                    item_db.Edit(i);
                }
                await item_db.Save();
            }
            location_db.Delete(location);
            await location_db.Save();
            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}