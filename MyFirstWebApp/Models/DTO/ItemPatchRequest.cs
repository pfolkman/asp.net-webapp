﻿using DAL.Models.DTO;
using MyFirstWebApp.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models.DTO
{
    public class ItemPatchRequest : IPatchRequest<Item>
    {

        public int? LocationID { get; set; }

        public Location Location { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int? Quantity { get; set; }

        public DateTime? Expires { get; set; }

        public DateTime? Updated { get; set; }

        public Item ToDB(Item existing)
        {
            existing.LocationID = LocationID ?? existing.LocationID;
            existing.Location = Location ?? existing.Location;
            existing.Name = Name ?? existing.Name;
            existing.Description = Description ?? existing.Description;
            existing.Quantity = Quantity ?? existing.Quantity;
            existing.Expires = Expires ?? existing.Expires;
            existing.Updated = DateTime.Now;
            return existing;
        }
    }
}