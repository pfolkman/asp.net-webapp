﻿using DAL.Models.DTO;
using MyFirstWebApp.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models.DTO
{
    public class ItemResponse : IResponse<Item>
    {
        public int ID { get; set; }

        public int? LocationID { get; set; }

        public Location location { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public DateTime? Expires { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }

        public void ParseExisting(Item existing)
        {
            ID = existing.ID;
            LocationID = existing.LocationID;
            //location = existing.Location;
            Name = existing.Name;
            Description = existing.Description;
            Quantity = existing.Quantity;
            Expires = existing.Expires;
            Created = existing.Created; //Initialize here maybe?
            Updated = existing.Updated; //Initialize here maybe?
        }
    }
}