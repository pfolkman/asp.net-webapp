﻿using DAL.Models.DTO;
using MyFirstWebApp.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models.DTO
{
    public class LocationRequest : IRequest<Location>
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }

        public Location ToDB()
        {
            return new Location
            {
                ID = this.ID,
                Name = this.Name,
                Description = this.Description
            };
        }
    }
}