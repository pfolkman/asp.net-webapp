﻿using DAL.Models.DTO;
using MyFirstWebApp.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models.DTO
{
    public class LocationPatchRequest : IPatchRequest<Location>
    {

        public String Name { get; set; }

        public String Description { get; set; }

        public Location ToDB(Location existing)
        {
            existing.Name = Name ?? existing.Name;
            existing.Description = Description ?? existing.Description;
            return existing;
        }
    }
}