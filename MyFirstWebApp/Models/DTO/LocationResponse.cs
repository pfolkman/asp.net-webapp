﻿using DAL.Models.DTO;
using MyFirstWebApp.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models.DTO
{
    public class LocationResponse : IResponse<Location>
    {

        public int ID { get; set; }
        public String Name { get; set;}
        public String Description { get; set; }


        public void ParseExisting(Location existing)
        {
            ID = existing.ID;
            Name = existing.Name;
            Description = existing.Description;
        }

        public Location ToDB()
        {
            throw new NotImplementedException();
        }
    }
}