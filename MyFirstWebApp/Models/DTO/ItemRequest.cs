﻿using DAL.Models.DTO;
using MyFirstWebApp.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models.DTO
{
    public class ItemRequest : IRequest<Item>
    {
        public int ID { get; set; }
        public int? LocationID { get; set; }

        public Location Location { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public DateTime? Expires { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Updated { get; set; }
        public Item ToDB()
        {
            return new Item
            {
                ID = this.ID,
                LocationID = this.LocationID,
                Location = this.Location,
                Name = this.Name,
                Description = this.Description,
                Quantity = this.Quantity,
                Expires = this.Expires,
                Created = DateTime.Now,
                Updated = DateTime.Now
            };
        }
    }
}