﻿using MyFirstWebApp.Migrations;
using MyFirstWebApp.Models.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext() : base("DefaultConnection")
        {
        }
        public virtual DbSet<Location> Locations { get; set; }

        public virtual DbSet<Item> Items { get; set; }

        //This method needed to allow azure to update the
        //database without manually adding migrations
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDBContext,Configuration>());
        }
    }
}