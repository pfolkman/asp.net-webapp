﻿using DAL.Models.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models.DB
{
    public class Item : IEntity
    {
        [Key]
        public int ID { get; set; }

        //[ForeignKey]
        public int? LocationID { get; set; }

        //[ForeignKey]
        //This is here to join the tables, Location is
        //Not apart of the database. 
        public virtual Location Location { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int Quantity { get; set; }

        public DateTime ? Expires { get; set; }

        public DateTime Created { get; set; }

        public DateTime ? Updated { get; set; }

    }
}