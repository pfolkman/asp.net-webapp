﻿using DAL.Models.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyFirstWebApp.Models.DB
{
    public class Location : IEntity
    {
        [Key]
        public int ID { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public virtual HashSet<Item> Items { get; set; } = new HashSet<Item>();
    }
}