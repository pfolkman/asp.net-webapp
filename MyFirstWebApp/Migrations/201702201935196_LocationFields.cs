namespace MyFirstWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LocationFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Locations", "Name", c => c.String());
            AddColumn("dbo.Locations", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Locations", "Description");
            DropColumn("dbo.Locations", "Name");
        }
    }
}
