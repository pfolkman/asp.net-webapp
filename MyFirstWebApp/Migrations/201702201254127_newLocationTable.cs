namespace MyFirstWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newLocationTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Tests", newName: "Locations");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Locations", newName: "Tests");
        }
    }
}
