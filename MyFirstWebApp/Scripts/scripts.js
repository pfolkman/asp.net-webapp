﻿(function () {
    var app = angular.module('myApp', ['MyApp.Services', 'angularModalService']);

    app.controller("ItemController", ['$scope', '$filter', 'ModalService', 'MyApp.Services.ItemService', 'MyApp.Services.LocationService', function ($scope, $filter, ModalService, ItemService, LocationService) {

        $scope.results = [];
        $scope.orderBy = "-Quantity";
        $scope.filter = "";

        $scope.showCreateModal = function () {
            ModalService.showModal({
                templateUrl: 'createmodal.html',
                controller: "ModalController"
            }).then(function (modal) {
                modal.element.modal();
                modal.close.then(function (result) {
                    $scope.message = "You said " + result;
                });
            });
        };

        $scope.$watch("itemFilter", function (newVal, oldVal) {
            if (newVal != "") {
                ItemService.query(function (results) {
                    $scope.results = $filter('filter')(results.Items,
                    function (value, index) {
                        return (value);
                    });
                });
            }
        });


        $scope.createItemFunc = function () {
            var newItem = {
                'Name': $scope.newItemName,
                'Quantity': $scope.newItemQuantity,
                'Description': $scope.newItemDesc,
                'LocationID': $scope.newItemLocation,
                'ExpirationDate': $scope.newItemExp
            }
            //newItem = JSON.stringify(newItem);
            ItemService.save(newItem);
            console.log("The following item has been created" + newItem);
            window.location.reload(true);
        }

        $scope.deleteItemFunc = function (id) {
            var itemID = id;
            var param = { itemId: itemID };
            ItemService.remove(param);
            console.log("Item " + itemID + " removed");
            window.location.reload(true);
        }

    }]);

    app.controller("LocationController", ['$scope', 'MyApp.Services.LocationService', function ($scope, LocationService) {


        $scope.createLocFunc = function () {
            var newLoc = {
                'Name': $scope.newLocName,
                'Description': $scope.newLocDesc
            }
            LocationService.save(newLoc);
            console.log(newLoc);
        }

    }]);

    app.controller('ModalController', function ($scope, close) {

        $scope.close = function (result) {
            close(result, 500); // close, but give 500ms for bootstrap to animate
        };

    });

    //Modal JS fiddle
    //https://jsfiddle.net/awolf2904/74exww04/
})();