﻿angular.module('myApp', ['ngResource'])
.controller("createModalController", function ($scope, $resource) {

    var dialog, form;

    var ItemService = $resource('http://trackinventory.azurewebsites.net/api/item/:itemId',
    { itemId: '@id' },
    {
        'get': { method: 'GET' },
        'query': { method: 'GET', isArray: false },
        'save': { method: 'POST' },
        'remove': { method: 'DELETE' }
    });

    dialog = $("#create-form").dialog({
        autoOpen: false,
        height: 400,
        width: 350,
        modal: true,
        buttons: {
            "Create Item": createItemFunc,
            Cancel: function () {
                dialog.dialog("close");
            }
        },
        close: function () {
            form[0].reset();
            allFields.removeClass("ui-state-error");
        }
    });

    form = dialog.find("form").on("submit", function (event) {
        event.preventDefault();
        createItemFunc();
    });

    $scope.createButtonClick = function () {
        console.log("Create Button clicked");
        dialog.dialog("open");
    }

    $scope.createItemFunc = function () {
        var newItem = {
            'Name': $scope.newItemName,
            'Quantity': $scope.newItemQuantity,
            'Description': $scope.newItemDesc,
            'LocationID': $scope.newItemLocation,
            'ExpirationDate': $scope.newItemExp
        }
        ItemService.save(newItem);
        console.log(newItem);
    }

});