﻿(function () {
    var app = angular.module('MyApp.Services', ['ngResource']);
    
    app.factory("MyApp.Services.ItemService", ['$resource', function ($resource) {
        return $resource(
                "http://trackinventory.azurewebsites.net/api/item/:itemId",
                { itemId: '@id' },
                {
                    query: {
                        method: "GET",
                        isArray: false
                    },
                    get: {
                        method: "GET"
                    },
                    save: {
                        method: "POST"
                    },
                    remove: {
                        method: "DELETE"
                    },
                    update: {
                        method: "PATCH"
                    }
                }

            );

    }]);

    app.factory("MyApp.Services.LocationService", ['$resource', function ($resource) {
        return $resource(
                "http://trackinventory.azurewebsites.net/api/location/:locationId",
                { locationId: '@id' },
                {
                    query: {
                        method: "GET",
                        isArray: false
                    },
                    get: {
                        method: "GET"
                    },
                    save: {
                        method: "POST"
                    },
                    remove: {
                        method: "DELETE"
                    },
                    update: {
                        method: "PATCH"
                    }
                }

            );

    }]);

})();